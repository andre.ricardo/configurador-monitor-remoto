package fpf.esdcontrol.configuradormonitor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static fpf.esdcontrol.configuradormonitor.R.id.edMqttPassword;
import static fpf.esdcontrol.configuradormonitor.R.id.edNetPassword;
import static fpf.esdcontrol.configuradormonitor.R.id.edPort;
import static fpf.esdcontrol.configuradormonitor.R.id.edSSID;
import static fpf.esdcontrol.configuradormonitor.R.id.edServer;
import static fpf.esdcontrol.configuradormonitor.R.id.edUser;

/**
 * Created by andre.ricardo on 12/01/2018.
 */
public class SettingsActivity extends Activity{

    private EditText txtSSID;
    private EditText txtNetPassword;
    private EditText txtServer;
    private EditText txtPort;
    private EditText txtMqttUser;
    private EditText txtMqttPassword;
    private Button btnSalvar;
    private Button btnLimpar;

    private static final String ARQUIVO_SETTINGS = "ArquivoSettings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        txtSSID = (EditText) findViewById(edSSID);
        txtNetPassword = (EditText) findViewById(edNetPassword);
        txtServer = (EditText) findViewById(edServer);
        txtPort = (EditText) findViewById(edPort);
        txtMqttUser = (EditText) findViewById(edUser);
        txtMqttPassword = (EditText) findViewById(edMqttPassword);
        btnSalvar = (Button) findViewById(R.id.btSalvar);
        btnLimpar = (Button) findViewById(R.id.btLimpar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_SETTINGS, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                if ((txtSSID.getText().toString().equals("")) ||
                        (txtNetPassword.getText().toString().equals("")) ||
                        (txtServer.getText().toString().equals("")) ||
                        (txtPort.getText().toString().equals("")) ||
                        (txtMqttUser.getText().toString().equals("")) ||
                        (txtMqttPassword.getText().toString().equals(""))){
                    Toast.makeText(SettingsActivity.this, "Informe todos os campos!", Toast.LENGTH_SHORT).show();
                }
                else {
                    editor.putString("ssid", txtSSID.getText().toString());
                    editor.putString("netPassword", txtNetPassword.getText().toString());
                    editor.putString("server", txtServer.getText().toString());
                    editor.putString("port", txtPort.getText().toString());
                    editor.putString("user", txtMqttUser.getText().toString());
                    editor.putString("mqttPassword", txtMqttPassword.getText().toString());
                    editor.commit();
                    finish();
                }
            }
        });

        btnLimpar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtSSID.setText("");
                txtNetPassword.setText("");
                txtServer.setText("");
                txtPort.setText("");
                txtMqttUser.setText("");
                txtMqttPassword.setText("");
            }
        });

        //Recuperar dados salvos
        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_SETTINGS, 0);
        if (sharedPreferences.contains("ssid")) {
            String textSSID = sharedPreferences.getString("ssid", "SSID");
            txtSSID.setText(textSSID);
        }
        if (sharedPreferences.contains("netPassword")) {
            String textNetPassword = sharedPreferences.getString("netPassword", "Net Password");
            txtNetPassword.setText(textNetPassword);
        }
        if (sharedPreferences.contains("server")) {
            String textServer = sharedPreferences.getString("server", "Server");
            txtServer.setText(textServer);
        }
        if (sharedPreferences.contains("port")){
            String textPort = sharedPreferences.getString("port", "Port");
            txtPort.setText(textPort);
        }
        if (sharedPreferences.contains("user")){
            String textMqttUser = sharedPreferences.getString("user", "MQTT User");
            txtMqttUser.setText(textMqttUser);
        }
        if (sharedPreferences.contains("mqttPassword")){
            String textMqttPassword = sharedPreferences.getString("mqttPassword", "MQTT Password");
            txtMqttPassword.setText(textMqttPassword);
        }
    }

    public void clickCancelar(View v){
        startActivity(new Intent(this, Main.class));
    }
}
