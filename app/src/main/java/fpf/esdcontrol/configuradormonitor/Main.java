package fpf.esdcontrol.configuradormonitor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static fpf.esdcontrol.configuradormonitor.R.id.btSettings;

public class Main extends Activity {

    private Button btnSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void openSettings(View v){
        startActivityForResult(new Intent(this, SettingsActivity.class), 1);
    }

    public void openPayload (View v){
        startActivityForResult(new Intent(this, PayloadActivity.class), 1);
    }
}
