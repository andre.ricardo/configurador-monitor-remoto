package fpf.esdcontrol.configuradormonitor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static fpf.esdcontrol.configuradormonitor.R.id.btLimparPayload;
import static fpf.esdcontrol.configuradormonitor.R.id.btSalvarPayload;
import static fpf.esdcontrol.configuradormonitor.R.id.edPayload;

/**
 * Created by André Ricardo on 26/01/2018.
 */
public class PayloadActivity extends Activity{

    private EditText txtPayload;
    private Button btnLimparPayload;
    private Button btnSalvarPayload;

    private static final String ARQUIVO_PAYLOAD = "ArquivoPayload";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payload);

        btnSalvarPayload = findViewById(btSalvarPayload);
        btnLimparPayload = findViewById(btLimparPayload);
        txtPayload = findViewById(edPayload);

        btnSalvarPayload.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PAYLOAD, 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                if (txtPayload.getText().toString().equals("")){
                    Toast.makeText(PayloadActivity.this, "Informe todos os campos!", Toast.LENGTH_SHORT).show();
                }
                else{
                    editor.putString("payload", txtPayload.getText().toString());
                    editor.commit();
                    finish();
                }
            }
        });

        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PAYLOAD, 0);
        if (sharedPreferences.contains("payload")){
            String textPayload = sharedPreferences.getString("payload", "Payload");
            txtPayload.setText(textPayload);
        }

        btnLimparPayload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                txtPayload.setText("");
            }
        });
    }

    public void clickCancelarPayload(View v){
        startActivity(new Intent(this, Main.class));
    }
}
